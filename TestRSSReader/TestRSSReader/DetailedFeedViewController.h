//
//  DetailedFeedViewController.h
//  TestRSSReader
//
//  Created by Artyom Belenkov on 3/1/16.
//  Copyright © 2016 abelenkov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailedFeedViewController : UIViewController

- (instancetype)initWithURL:(NSURL *)anURL;

@end
