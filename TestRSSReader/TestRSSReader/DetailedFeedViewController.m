//
//  DetailedFeedViewController.m
//  TestRSSReader
//
//  Created by Artyom Belenkov on 3/1/16.
//  Copyright © 2016 abelenkov. All rights reserved.
//

#import "DetailedFeedViewController.h"
#import "AlertViewController.h"

@interface DetailedFeedViewController ()

@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *
		activityIndicator;
@property (nonatomic, strong) NSURL *URL;
@property (nonatomic, assign) BOOL allowLoad;
@property (nonatomic, strong) AlertViewController *alertViewController;

@end

@implementation DetailedFeedViewController

static NSString *const kOpenInBrowserString = @"Open in Browser";

- (instancetype)initWithURL:(NSURL *)anURL
{
	self = [super init];
    
    if (nil != self)
    {
    	self.URL = anURL;
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.allowLoad = YES;
    [self.webView loadRequest:[NSURLRequest requestWithURL:self.URL]];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]
    		initWithTitle:kOpenInBrowserString style:UIBarButtonItemStylePlain
            target:self action:@selector(openInBrowser)];
}

- (void)viewWillDisappear:(BOOL)anAnimated
{
	[super viewWillDisappear:anAnimated];
    
    [self.webView stopLoading];
}

#pragma mark - UIWebViewDelegate methods

- (BOOL)webView:(UIWebView*)aWebView shouldStartLoadWithRequest:(NSURLRequest*)
		aRequest navigationType:(UIWebViewNavigationType)aNavigationType
{
    return self.allowLoad;
}

- (void)webViewDidFinishLoad:(UIWebView*)aWebView
{
    self.allowLoad = NO;
    [self.activityIndicator stopAnimating];
}

- (void)webView:(UIWebView *)aWebView didFailLoadWithError:(NSError *)anError
{
	if (NSURLErrorCancelled != anError.code)
    {
        [self.activityIndicator stopAnimating];
        
        __weak typeof(self) theWeakSelf = self;
        self.alertViewController = [[AlertViewController alloc]
                initWithRetryHandler:
        ^{
            theWeakSelf.allowLoad = YES;
            [theWeakSelf.activityIndicator startAnimating];
            [theWeakSelf.webView loadRequest:
                    [NSURLRequest requestWithURL:theWeakSelf.URL]];
            theWeakSelf.alertViewController = nil;
        } cancelHandler:NULL];
        
        [self.alertViewController presentAlertViewWithTitle:@"Error title"
        		message:@"Error message" inController:self];
    }
}

#pragma mark -

- (void)openInBrowser
{
	[[UIApplication sharedApplication] openURL:self.URL];
}

@end
