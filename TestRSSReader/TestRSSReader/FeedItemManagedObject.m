//
//  FeedItemManagedObject.m
//  TestRSSReader
//
//  Created by Artyom Belenkov on 3/2/16.
//  Copyright © 2016 abelenkov. All rights reserved.
//

#import "FeedItemManagedObject.h"
#import "FeedItem.h"

@implementation FeedItemManagedObject

@synthesize title, descriptionText, linkString, pubDate, thumbnailURLString,
		thumbnailHeight, thumbnailWidth, imageData;

@end
