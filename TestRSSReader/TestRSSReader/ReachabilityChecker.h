//
//  ReachabilityChecker.h
//  TestRSSReader
//
//  Created by Artyom Belenkov on 3/2/16.
//  Copyright © 2016 abelenkov. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const kReachabilityNotification;
extern NSString *const kReachableKey;

@interface ReachabilityChecker : NSObject

@property (nonatomic, assign) BOOL shouldNotify;

+ (ReachabilityChecker *)sharedInstance;
+ (BOOL)checkNetworkAvailable;

- (void)startChecking;
- (void)stopChecking;

@end
