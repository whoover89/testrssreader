//
//  AppDelegate.m
//  TestRSSReader
//
//  Created by Artyom Belenkov on 2/29/16.
//  Copyright © 2016 abelenkov. All rights reserved.
//

#import "AppDelegate.h"
#import "NewsViewController.h"
#import "ReachabilityChecker.h"

@interface AppDelegate ()
@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)anApplication
        didFinishLaunchingWithOptions:(NSDictionary *)aLaunchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
	
	UINavigationController *theController = [[UINavigationController alloc]
    		initWithRootViewController:[NewsViewController new]];
	theController.navigationBar.translucent = NO;
    
    [[ReachabilityChecker sharedInstance] startChecking];
	
    NSString *theRSSURL = [[NSUserDefaults standardUserDefaults]
    		objectForKey:@"RSS_URL_IDENTIFIER"];
    if (nil == theRSSURL)
    {
    	[[NSUserDefaults standardUserDefaults] setObject:
        		@"http://feeds.bbci.co.uk/news/rss.xml"
                forKey:@"RSS_URL_IDENTIFIER"];
    }
    
	self.window.rootViewController = theController;
	[self.window makeKeyAndVisible];

	return YES;
}

- (void)applicationWillResignActive:(UIApplication *)anApplication
{
	[[ReachabilityChecker sharedInstance] stopChecking];
}

- (void)applicationWillEnterForeground:(UIApplication *)anApplication
{
    [[ReachabilityChecker sharedInstance] startChecking];
}

@end
