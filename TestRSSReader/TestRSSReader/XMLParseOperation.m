//
//  XMLParseOperation.m
//  TestRSSReader
//
//  Created by Artyom Belenkov on 2/29/16.
//  Copyright © 2016 abelenkov. All rights reserved.
//

#import "XMLParseOperation.h"
#import "FeedItem.h"
#import "CoreDataManager.h"

@interface XMLParseOperation ()

@property (nonatomic, strong) NSData *data;
@property (nonatomic, strong) NSXMLParser *XMLParser;
@property (nonatomic, strong) NSMutableDictionary *currentFeedItemDictionary;
@property (nonatomic, strong) NSMutableArray<FeedItem *> *feedItems;
@property (nonatomic, strong) NSString *currentFeedAttributeName;
@property (nonatomic, copy) XMLParseOperationCompletion completion;

@end

@implementation XMLParseOperation

static NSString *const kXMLElementItem = @"item";
static NSString *const kXMLElementMediaThumbnail = @"media:thumbnail";

@synthesize feedItems;

- (NSMutableArray *)feedItems
{
	if (nil == feedItems)
    {
    	feedItems = [NSMutableArray new];
    }
    
    return feedItems;
}

- (instancetype)initWithData:(NSData *)aData completionBlock:
		(XMLParseOperationCompletion)aCompletion;
{
	self = [super init];
    
    if (nil != self)
    {
    	self.data = aData;
        self.completion = aCompletion;
    }
    
    return self;
}

- (void)main
{
	self.XMLParser = [[NSXMLParser alloc] initWithData:self.data];
    self.XMLParser.delegate = self;
    [self.XMLParser parse];
}

#pragma mark - NSXMLParserDelegate methods

- (void)parser:(NSXMLParser *)aParser didStartElement:(NSString *)anElementName
        namespaceURI:(NSString *)aNamespaceURI qualifiedName:(NSString *)aName
        attributes:(NSDictionary *)anAttributes
{
    if ([kXMLElementItem isEqualToString:anElementName])
    {
    	self.currentFeedItemDictionary = [NSMutableDictionary new];
    }
    else if ([kXMLElementMediaThumbnail isEqualToString:anElementName])
    {
    	self.currentFeedItemDictionary[anElementName] = anAttributes;
    }
    else
    {
    	self.currentFeedAttributeName = anElementName;
    }
}

- (void)parser:(NSXMLParser *)aParser didEndElement:(NSString *)anElementName
		namespaceURI:(NSString *)aNamespaceURI qualifiedName:(NSString *)aName
{
	if (nil != self.currentFeedItemDictionary)
    {
    	if ([kXMLElementItem isEqualToString:anElementName])
        {
        	FeedItem *theFeedItem = [[FeedItem alloc] initWithDictionary:
            		self.currentFeedItemDictionary];
            [self.feedItems addObject:theFeedItem];
            self.currentFeedItemDictionary = nil;
        }
    }
    
    self.currentFeedAttributeName = nil;
}

- (void)parser:(NSXMLParser *)aParser foundCharacters:(NSString *)aString
{
	if (nil != self.currentFeedAttributeName)
    {
    	self.currentFeedItemDictionary[self.currentFeedAttributeName] = aString;
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)aParser
{
    if (NULL != self.completion)
    {
        self.completion(self.feedItems, nil);
        self.completion = NULL;
    }
}


- (void)parser:(NSXMLParser *)aParser parseErrorOccurred:(NSError *)aParseError
{
	if (NULL != self.completion)
    {
    	self.completion(nil, aParseError);
        self.completion = NULL;
    }
}

@end
