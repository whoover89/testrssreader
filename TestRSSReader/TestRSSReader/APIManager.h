//
//  APIManager.h
//  Test
//
//  Created by Artyom Belenkov on 2/29/16.
//  Copyright © 2016 abelenkov. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^FeedNewsRequestCompletion)(NSData *inData, NSError *inError,
		NSURLSessionTask *inTask);
typedef void (^ImageRequestCompletion)(NSData *inImageData, NSError *inError,
        NSURLSessionTask *aTask);

@interface APIManager : NSObject

+ (APIManager *)sharedInstance;

- (NSURLSessionTask *)getThumbnailWithURL:(NSURL *)anURL
		completion:(ImageRequestCompletion)aCompletion;
- (NSURLSessionTask *)getFeedNewsWithCompletion:(FeedNewsRequestCompletion)
		aCompletion;

@end
