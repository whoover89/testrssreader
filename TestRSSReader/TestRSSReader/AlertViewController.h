//
//  AlertViewController.h
//  TestRSSReader
//
//  Created by Artyom Belenkov on 3/2/16.
//  Copyright © 2016 abelenkov. All rights reserved.
//

#import <UIKit/UIKit.h>

#define IS_IOS7 [[UIDevice currentDevice].systemVersion hasPrefix:@"7"]

typedef void (^AlertViewControllerHandler)();

@interface AlertViewController : NSObject

- (instancetype)initWithRetryHandler:(AlertViewControllerHandler)aRetryHandler
		cancelHandler:(AlertViewControllerHandler)aCancelHandler;

- (void)presentAlertViewWithTitle:(NSString *)aTitle message:(NSString *)
		aMessage inController:(UIViewController *)aController;

@end
