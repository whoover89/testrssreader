//
//  CoreDataManager.m
//  TestRSSReader
//
//  Created by Artyom Belenkov on 3/1/16.
//  Copyright © 2016 abelenkov. All rights reserved.
//

#import "CoreDataManager.h"
#import "FeedItem.h"
#import "FeedItemManagedObject.h"
#import <CoreData/CoreData.h>

@interface CoreDataManager ()

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, strong) NSPersistentStoreCoordinator *
		persistentStoreCoordinator;

@end

@implementation CoreDataManager

static NSString *const kFeedItemIdentifier = @"FeedItemManagedObject";
static NSString *const kApplicationName = @"TestRSSReader";
static NSString *const kCoreDataExtension = @"momd";

@synthesize managedObjectContext;
@synthesize managedObjectModel;
@synthesize persistentStoreCoordinator;

+ (CoreDataManager *)defaultManager
{
	static CoreDataManager *theInstance = nil;
    static dispatch_once_t theOnceToken;
    
    dispatch_once(&theOnceToken,
    ^{
        theInstance = [CoreDataManager new];
    });
    
    return theInstance;

}

- (NSString *)databaseName
{
	return [NSString stringWithFormat:@"%@%@", kApplicationName, @".sqlite"];
}

- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
    		inDomains:NSUserDomainMask] lastObject];
}

- (NSURL *)storeURL
{
	return [self.applicationDocumentsDirectory URLByAppendingPathComponent:
    		self.databaseName];
}

- (NSManagedObjectModel *)managedObjectModel
{
    if (nil == managedObjectModel)
    {
        NSURL *theModelURL = [[NSBundle mainBundle] URLForResource:
        		kApplicationName withExtension:kCoreDataExtension];
        managedObjectModel = [[NSManagedObjectModel alloc]
        		initWithContentsOfURL:theModelURL];
    }
    
    return managedObjectModel;
}

- (NSManagedObjectContext *)managedObjectContext
{
    if (nil == managedObjectContext)
    {
        NSPersistentStoreCoordinator *theCoordinator =
        		self.persistentStoreCoordinator;
        if (nil != theCoordinator)
        {
            managedObjectContext = [[NSManagedObjectContext alloc]
            		initWithConcurrencyType:NSMainQueueConcurrencyType];
            [managedObjectContext setPersistentStoreCoordinator:theCoordinator];
        }
    }
    
    return managedObjectContext;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (nil == persistentStoreCoordinator)
    {
    	persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc]
        		initWithManagedObjectModel:self.managedObjectModel];
        NSError *theError = nil;

        if (![persistentStoreCoordinator addPersistentStoreWithType:
                NSSQLiteStoreType configuration:nil URL:self.storeURL
                options:nil error:&theError])
        {
            NSLog(@"Unresolved error %@, %@", theError, theError.userInfo);
            abort();
        }
    }
    
    return persistentStoreCoordinator;
}

#pragma mark -

- (NSArray<FeedItem *> *)getSavedFeedItems
{
	NSManagedObjectContext *theManagedObjectContext = self.managedObjectContext;
    
    NSFetchRequest *theFetchRequest = [[NSFetchRequest alloc]
    		initWithEntityName:kFeedItemIdentifier];
    
    NSArray<FeedItemManagedObject *> *theEntetiesArray =
    		[theManagedObjectContext executeFetchRequest:
    		theFetchRequest error:nil];
    
    NSMutableArray<FeedItem *> *theFeedItems = [NSMutableArray new];
    [theEntetiesArray enumerateObjectsUsingBlock:
    ^(FeedItemManagedObject *outObject, NSUInteger outIndex, BOOL *outStop)
    {
        [theFeedItems addObject:[[FeedItem alloc] initWithFeedItemManagedObject:
        		outObject]];
    }];
    
    return theFeedItems;
}

- (void)saveFeedItems:(NSArray<FeedItem *> *)anItems
{
	NSManagedObjectContext *theContext = self.managedObjectContext;

	for (FeedItem *theFeedItem in anItems)
    {
    	FeedItemManagedObject *theObject = [NSEntityDescription
        		insertNewObjectForEntityForName:kFeedItemIdentifier
                inManagedObjectContext:theContext];
        
        theObject.title = theFeedItem.title;
        theObject.descriptionText = theFeedItem.descriptionText;
        theObject.linkString = theFeedItem.linkString;
        theObject.pubDate = theFeedItem.thumbnailURLString;
        theObject.thumbnailHeight = @(theFeedItem.thumbnailHeight);
        theObject.thumbnailWidth = @(theFeedItem.thumbnailWidth);
        theObject.imageData = theFeedItem.imageData;
	}
    
    [self saveContext];
}

- (void)removeAllObjects
{
	NSManagedObjectContext *theContext = self.managedObjectContext;
	NSFetchRequest *theAllFeedItems = [NSFetchRequest new];
	[theAllFeedItems setEntity:[NSEntityDescription entityForName:
    		kFeedItemIdentifier inManagedObjectContext:theContext]];
    [theAllFeedItems setIncludesPropertyValues:NO];

    NSError *theError = nil;
    NSArray<FeedItemManagedObject *> *theFeedItems = [theContext
    		executeFetchRequest: theAllFeedItems error:&theError];
    for (NSManagedObject *theFeedItem in theFeedItems)
    {
      [theContext deleteObject:theFeedItem];
    }
	
    [self saveContext];
}

#pragma mark - Core Data Saving support

- (void)saveContext
{
    NSManagedObjectContext *theManagedObjectContext = self.managedObjectContext;
    if (nil != theManagedObjectContext)
    {
        NSError *theError = nil;
        if (theManagedObjectContext.hasChanges &&
        		![theManagedObjectContext save:&theError])
        {
            NSLog(@"Unresolved error %@, %@", theError, theError.userInfo);
            abort();
        }
    }
}

@end
