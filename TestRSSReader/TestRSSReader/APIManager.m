
//
//  APIManager.m
//  Test
//
//  Created by Artyom Belenkov on 2/29/16.
//  Copyright © 2016 abelenkov. All rights reserved.
//

#import "APIManager.h"
#import "FeedItem.h"

@interface APIManager()

@property (nonatomic, strong) NSURLSession *session;

@end

@implementation APIManager

static NSString *const kRSSURLIdentifier = @"RSS_URL_IDENTIFIER";

+ (APIManager *)sharedInstance
{
    static APIManager *theInstance = nil;
    static dispatch_once_t theOnceToken;
    
    dispatch_once(&theOnceToken,
    ^{
        theInstance = [APIManager new];
    });
    
    return theInstance;
}

- (instancetype)init
{
	self = [super init];
    
    if (nil != self)
    {
    	self.session = [NSURLSession sharedSession];
    }
    
    return self;
}

- (NSURLSessionTask *)getThumbnailWithURL:(NSURL *)anURL
		completion:(ImageRequestCompletion)aCompletion
{
	NSURLSessionTask *theTask = [self.session dataTaskWithURL:anURL
    	completionHandler:
    ^(NSData *inData, NSURLResponse *inResponse, NSError *inError)
    {
        aCompletion(inData, inError, theTask);
    }];
    [theTask resume];
    
    return theTask;
}

- (NSURLSessionTask *)getFeedNewsWithCompletion:(FeedNewsRequestCompletion)
		aCompletion
{
	NSURL *theRSSURL = [NSURL URLWithString:
    		[[NSUserDefaults standardUserDefaults]
            objectForKey:kRSSURLIdentifier]];
    
	NSURLSessionTask *theTask = [self.session dataTaskWithURL:theRSSURL
    		completionHandler:
    ^(NSData *inData, NSURLResponse *inResponse, NSError *inError)
    {
        aCompletion(inData, inError, theTask);
    }];
    [theTask resume];
    
    return theTask;
}

@end
