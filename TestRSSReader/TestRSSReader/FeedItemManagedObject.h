//
//  FeedItemManagedObject.h
//  TestRSSReader
//
//  Created by Artyom Belenkov on 3/2/16.
//  Copyright © 2016 abelenkov. All rights reserved.
//

#import <CoreData/CoreData.h>
#import <UIKit/UIKit.h>

@class FeedItem;

@interface FeedItemManagedObject : NSManagedObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *descriptionText;
@property (nonatomic, strong) NSString *linkString;
@property (nonatomic, strong) NSString *pubDate;
@property (nonatomic, strong) NSString *thumbnailURLString;
@property (nonatomic, strong) NSNumber *thumbnailHeight;
@property (nonatomic, strong) NSNumber *thumbnailWidth;
@property (nonatomic, strong) NSData *imageData;

@end
