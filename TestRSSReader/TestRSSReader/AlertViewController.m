//
//  AlertViewController.m
//  TestRSSReader
//
//  Created by Artyom Belenkov on 3/2/16.
//  Copyright © 2016 abelenkov. All rights reserved.
//

#import "AlertViewController.h"

@interface AlertViewController() <UIAlertViewDelegate>

@property (nonatomic, copy) AlertViewControllerHandler cancelHandler;
@property (nonatomic, copy) AlertViewControllerHandler retryHandler;

@end

@implementation AlertViewController

- (instancetype)initWithRetryHandler:(AlertViewControllerHandler)aRetryHandler
		cancelHandler:(AlertViewControllerHandler)aCancelHandler
{
	self = [super init];
    
    if (nil != self)
    {
    	self.cancelHandler = aCancelHandler;
        self.retryHandler = aRetryHandler;
    }
    
    return self;
}

- (void)presentAlertViewWithTitle:(NSString *)aTitle message:(NSString *)
		aMessage inController:(UIViewController *)aController
{
#ifdef IS_IOS7
	UIAlertView *theAlertView = [[UIAlertView alloc] initWithTitle:aTitle
    		message:aMessage delegate:self cancelButtonTitle:@"Cancel"
            otherButtonTitles:@"Retry", nil];
	[theAlertView show];
#else
	UIAlertAction *theRetryAction = [UIAlertAction actionWithTitle:@"Retry"
    		style:UIAlertActionStyleDefault handler:
    ^(UIAlertAction * _Nonnull action)
    {
        if (NULL != self.retryHandler)
        {
        	self.retryHandler();
        }
    }];
    
    UIAlertAction *theCancelAction = [UIAlertAction actionWithTitle:@"Cancel"
    		style:UIAlertActionStyleDefault handler:
    ^(UIAlertAction * _Nonnull action)
    {
        if (NULL != self.cancelHandler)
        {
        	self.cancelHandler();
        }
    }];
    
	UIAlertController *theAlertController = [UIAlertController
    		alertControllerWithTitle:aTitle message:aMessage
            preferredStyle:UIAlertControllerStyleAlert];
    [theAlertController addAction:theCancelAction];
    [theAlertController addAction:theRetryAction];
    
    [aController presentViewController:theAlertController animated:YES
    		completion:NULL];
#endif
}

- (void)alertView:(UIAlertView *)anAlertView
		clickedButtonAtIndex:(NSInteger)aButtonIndex
{
	if (0 == aButtonIndex && NULL != self.cancelHandler)
    {
    	self.cancelHandler();
    }
    else if (NULL != self.retryHandler)
    {
    	self.retryHandler();
    }
}

@end
