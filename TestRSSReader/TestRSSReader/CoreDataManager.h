//
//  CoreDataManager.h
//  TestRSSReader
//
//  Created by Artyom Belenkov on 3/1/16.
//  Copyright © 2016 abelenkov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FeedItem;

@interface CoreDataManager : NSObject

+ (CoreDataManager *)defaultManager;

- (NSArray<FeedItem *> *)getSavedFeedItems;
- (void)saveFeedItems:(NSArray<FeedItem *> *)anItems;

- (void)saveContext;
- (void)removeAllObjects;

@end
