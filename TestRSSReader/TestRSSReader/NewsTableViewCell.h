//
//  NewsTableViewCell.h
//  TestRSSReader
//
//  Created by Artyom Belenkov on 3/1/16.
//  Copyright © 2016 abelenkov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *descriptionTextLabel;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic, strong) UIImage *thumbnailImage;

@property (nonatomic, strong) NSURLSessionTask *task;

@end
