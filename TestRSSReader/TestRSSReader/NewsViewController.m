//
//  NewsViewController.m
//  TestRSSReader
//
//  Created by Artyom Belenkov on 2/29/16.
//  Copyright © 2016 abelenkov. All rights reserved.
//

#import "NewsViewController.h"
#import "NewsTableViewCell.h"
#import "DetailedFeedViewController.h"
#import "AlertViewController.h"
#import "XMLParseOperation.h"
#import "ReachabilityChecker.h"

#import "APIManager.h"
#import "CoreDataManager.h"
#import "FeedItem.h"

@interface NewsViewController () <UITableViewDataSource, UITableViewDelegate,
		UISearchBarDelegate>

@property (strong, nonatomic) IBOutlet UITableView *newsTableView;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *topSearchBarConstraint;

@property (nonatomic, strong) NSArray<FeedItem *> *feedItems;
@property (nonatomic, strong) NSMutableArray<FeedItem *> *searchResults;
@property (nonatomic, strong) NSMutableSet<NSURLSessionTask *> *dataTasks;
@property (nonatomic, strong) NSOperationQueue *XMLParseOperationQueue;
@property (nonatomic, strong) AlertViewController *alertViewController;

@end

@implementation NewsViewController

static NSString *const kCellReuseIdentifier = @"kCellReuseIdentifier";
static NSString *const kCellNibName = @"NewsTableViewCell";
static NSString *const kTitle = @"RSS Reader";

- (void)dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
    [self cancellAllDataTasks];
    [self.XMLParseOperationQueue cancelAllOperations];
}

- (void)viewDidLoad
{
	[super viewDidLoad];
    
    self.XMLParseOperationQueue = [NSOperationQueue new];
    self.dataTasks = [NSMutableSet new];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
            selector:@selector(applicationWillTerminate:)
            name:UIApplicationWillTerminateNotification
            object:[UIApplication sharedApplication]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
            selector:@selector(reachabilityDidChange:)
            name:kReachabilityNotification
            object:nil];
    
    self.title = kTitle;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]
            initWithBarButtonSystemItem:UIBarButtonSystemItemSearch
            target:self action:@selector(showHideSearchBar)];
    
    UINib *theNib = [UINib nibWithNibName:kCellNibName
    		bundle:[NSBundle mainBundle]];
    [self.newsTableView registerNib:theNib forCellReuseIdentifier:
            kCellReuseIdentifier];
    self.newsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self getNewsFeed];
}

- (void)getNewsFeed
{
	__weak typeof(self) theWeakSelf = self;
    [[APIManager sharedInstance] getFeedNewsWithCompletion:
    ^(NSData *inData, NSError *inError, NSURLSessionTask *outTask)
    {
        if (nil == inError && nil != inData)
        {
            [theWeakSelf parseXMLFromData:inData];
        }
        else
        {
        	[theWeakSelf handleError:inError];
        }
        
        [theWeakSelf removeDataTask:outTask];
    }];
}

- (void)handleError:(NSError *)anError
{
	BOOL theShouldShowAlert = YES;
	if (![ReachabilityChecker checkNetworkAvailable])
    {
        self.feedItems = [[CoreDataManager defaultManager]
                getSavedFeedItems];
        
        if (0 < self.feedItems.count)
        {
        	theShouldShowAlert = NO;
        }
    }
	
    dispatch_async(dispatch_get_main_queue(),
    ^{
    	if (!theShouldShowAlert)
        {
        	[self.newsTableView reloadData];
        }
        else
        {
        	__weak typeof(self) theWeakSelf = self;
        	self.alertViewController = [[AlertViewController
            		alloc] initWithRetryHandler:
            ^{
            	[theWeakSelf getNewsFeed];
                theWeakSelf.alertViewController = nil;
            } cancelHandler:
            ^{
            	theWeakSelf.alertViewController = nil;
            }];
            
            [self.alertViewController presentAlertViewWithTitle:@"Error"
            		message:@"Can't load feed" inController:self];
        }
    });
}

- (void)cancellAllDataTasks
{
	@synchronized(self)
    {
        NSSet *theTasks = [NSSet setWithSet:self.dataTasks];
        for (NSURLSessionTask *theTask in theTasks)
        {
        	[theTask cancel];
        }
    	[self.dataTasks removeAllObjects];
    }
}

- (void)removeDataTask:(NSURLSessionTask *)aTask
{
	@synchronized(self)
    {
        if (nil != aTask)
        {
            [self.dataTasks removeObject:aTask];
        }
    }
}

- (void)parseXMLFromData:(NSData *)aData
{
	__weak typeof(self) theWeakSelf = self;
	XMLParseOperation *theOperation = [[XMLParseOperation alloc]
            initWithData:aData completionBlock:
    ^(NSArray<FeedItem *> *outFeedItems, NSError *outError)
    {
        theWeakSelf.feedItems = outFeedItems;
        dispatch_async(dispatch_get_main_queue(),
        ^{
            [theWeakSelf.newsTableView reloadData];
        });
    }];
    
    [self.XMLParseOperationQueue addOperation:theOperation];
}

- (void)showHideSearchBar
{
	[self.view endEditing:YES];
    
	CGFloat theConstant = self.topSearchBarConstraint.constant;
    __weak typeof(self) theWeakSelf = self;
	[UIView animateWithDuration:0.5 animations:
	^{
		theWeakSelf.topSearchBarConstraint.constant = theConstant <
        		0 ? 0.0f : -44.0f;
		[self.view layoutIfNeeded];
	}];
}

#pragma mark - NSNotificationCenter

- (void)applicationWillTerminate:(NSNotification *)aNotification
{
	[[CoreDataManager defaultManager] removeAllObjects];
	[[CoreDataManager defaultManager] saveFeedItems:self.feedItems];
}

- (void)reachabilityDidChange:(NSNotification *)aNotification
{
	BOOL theReachable = [aNotification.userInfo[kReachableKey] boolValue];
    
    if (!theReachable)
    {
    	__weak typeof(self) theWeakSelf = self;
    	self.alertViewController = [[AlertViewController
        		alloc] initWithRetryHandler:
        ^{
        	[ReachabilityChecker sharedInstance].shouldNotify = YES;
            theWeakSelf.alertViewController = nil;
        }
        cancelHandler:
        ^{
        	[ReachabilityChecker sharedInstance].shouldNotify = YES;
            theWeakSelf.alertViewController = nil;
        }];
        
        [self.alertViewController presentAlertViewWithTitle:
        		@"Problem with Internet" message:@"Is not reachable"
                inController:self];
    }
}

#pragma mark - UITableViewDataSource methods

- (NSInteger)tableView:(UITableView *)tableView
        numberOfRowsInSection:(NSInteger)aSection
{
	if (self.isSearch)
    {
        return self.searchResults.count;
    }
	return self.feedItems.count;
}

#pragma mark - UITableViewDelegate methods

- (UITableViewCell *)tableView:(UITableView *)aTableView
        cellForRowAtIndexPath:(NSIndexPath *)anIndexPath
{
	NewsTableViewCell *theCell = [aTableView dequeueReusableCellWithIdentifier:
            kCellReuseIdentifier forIndexPath:anIndexPath];
    
    if (nil == theCell)
    {
        theCell = (NewsTableViewCell *)[[UITableViewCell alloc]
        		initWithStyle:UITableViewCellStyleDefault
                reuseIdentifier:kCellReuseIdentifier];
    }
    
    FeedItem *theFeedItem = nil;
    if (self.isSearch)
    {
        theFeedItem = self.searchResults[anIndexPath.row];
    }
    else
    {
    	theFeedItem = self.feedItems[anIndexPath.row];
    }
    
    [theCell.task cancel];
    [self removeDataTask:theCell.task];
    
    theCell.thumbnailImage = nil;
    
    theCell.titleLabel.text = theFeedItem.title;
    theCell.descriptionTextLabel.text = theFeedItem.descriptionText;
    
    if (nil == theFeedItem.imageData)
    {
        if (nil != theFeedItem.thumbnailURLString)
        {
        	[theCell.activityIndicator startAnimating];
            
            __weak typeof(theCell) theWeakCell = theCell;
            __weak typeof(self) theWeakSelf = self;
            [[APIManager sharedInstance] getThumbnailWithURL:
                    [NSURL URLWithString:theFeedItem.thumbnailURLString]
                    completion:
            ^(NSData *inImageData, NSError *inError, NSURLSessionTask *inTask)
            {
                dispatch_async(dispatch_get_main_queue(),
                ^{
                    theWeakCell.thumbnailImage = [UIImage imageWithData:
                    		inImageData];
                    theFeedItem.imageData = inImageData;
                });
                
                [theWeakSelf removeDataTask:inTask];
            }];
        }
    }
    else
    {
    	theCell.thumbnailImage = [UIImage imageWithData:theFeedItem.imageData];
    }
	    
	return theCell;
}

- (void)tableView:(UITableView *)aTableView
        didSelectRowAtIndexPath:(NSIndexPath *)anIndexPath
{
	[aTableView deselectRowAtIndexPath:anIndexPath animated:YES];
    
    FeedItem *theFeedItem = self.feedItems[anIndexPath.row];
    
    DetailedFeedViewController *theController = [[DetailedFeedViewController
    		alloc] initWithURL:[NSURL URLWithString:theFeedItem.linkString]];
    
    [self.navigationController pushViewController:theController animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:
		(NSIndexPath *)indexPath
{
	FeedItem *theFeedItem = self.feedItems[indexPath.row];
    CGFloat theHeight = theFeedItem.thumbnailHeight > 0 ?
    		theFeedItem.thumbnailHeight + 20 : 64.0;

	return theHeight;
}

#pragma mark - UISearchBarDelegate methods

- (void)searchBar:(UISearchBar *)aSearchBar
		textDidChange:(NSString *)aSearchText
{
	if (0 < aSearchText.length)
    {
		[self updateFilteredContentForString:aSearchText];
    }
    else
    {
    	self.searchResults = nil;
    }
    
    [self.newsTableView reloadData];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)aSearchBar
{
	self.searchResults = nil;
}

#pragma mark -

- (void)updateFilteredContentForString:(NSString *)aString
{
    self.searchResults = [NSMutableArray new];
    
    for (FeedItem *theFeedItem in self.feedItems)
    {
        if ([theFeedItem.title rangeOfString:aString
        		options:NSCaseInsensitiveSearch | NSDiacriticInsensitiveSearch
                range:NSMakeRange(0, [theFeedItem.title length])].length > 0)
    	{
            [self.searchResults addObject:theFeedItem];
        }
    }
}

- (BOOL)isSearch
{
	return nil != self.searchResults;
}

@end
