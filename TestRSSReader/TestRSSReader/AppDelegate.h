//
//  AppDelegate.h
//  TestRSSReader
//
//  Created by Artyom Belenkov on 2/29/16.
//  Copyright © 2016 abelenkov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

