//
//  FeedItem.h
//  TestRSSReader
//
//  Created by Artyom Belenkov on 3/1/16.
//  Copyright © 2016 abelenkov. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FeedItemManagedObject;

@interface FeedItem : NSObject

@property (nonatomic, readonly) NSString *title;
@property (nonatomic, readonly) NSString *descriptionText;
@property (nonatomic, readonly) NSString *linkString;
@property (nonatomic, readonly) NSString *pubDate;
@property (nonatomic, readonly) NSString *thumbnailURLString;
@property (nonatomic, readonly) CGFloat thumbnailHeight;
@property (nonatomic, readonly) CGFloat thumbnailWidth;
@property (nonatomic, strong) NSData *imageData;

- (instancetype)initWithDictionary:(NSDictionary *)aDictionary;
- (instancetype)initWithFeedItemManagedObject:(FeedItemManagedObject *)anObject;

@end
