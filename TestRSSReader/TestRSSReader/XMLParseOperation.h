//
//  XMLParseOperation.h
//  TestRSSReader
//
//  Created by Artyom Belenkov on 2/29/16.
//  Copyright © 2016 abelenkov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FeedItem;

typedef void (^XMLParseOperationCompletion)(NSArray<FeedItem *> *outFeedItems,
		NSError *outError);

@interface XMLParseOperation : NSOperation <NSXMLParserDelegate>

- (instancetype)initWithData:(NSData *)aData completionBlock:
		(XMLParseOperationCompletion)aCompletion;

@end
