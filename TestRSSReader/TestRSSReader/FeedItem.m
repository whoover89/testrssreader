//
//  FeedItem.m
//  TestRSSReader
//
//  Created by Artyom Belenkov on 3/1/16.
//  Copyright © 2016 abelenkov. All rights reserved.
//

#import "FeedItem.h"
#import "FeedItemManagedObject.h"

@interface FeedItem()

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *descriptionText;
@property (nonatomic, strong) NSString *linkString;
@property (nonatomic, strong) NSString *pubDate;
@property (nonatomic, strong) NSString *thumbnailURLString;
@property (nonatomic, assign) CGFloat thumbnailHeight;
@property (nonatomic, assign) CGFloat thumbnailWidth;

@end

@implementation FeedItem

static NSString *const kTitleKey = @"title";
static NSString *const kDescriptionKey = @"description";
static NSString *const kPubDateKey = @"pubDate";
static NSString *const kGuidKey = @"guid";
static NSString *const kMediaThumbnailKey = @"media:thumbnail";
static NSString *const kURLKey = @"url";
static NSString *const kWidthKey = @"width";
static NSString *const kHeightKey = @"height";

- (instancetype)initWithDictionary:(NSDictionary *)aDictionary
{
    self = [super init];
    
    if (nil != self)
    {
    	self.title = aDictionary[kTitleKey];
        self.descriptionText = aDictionary[kDescriptionKey];
        self.pubDate = aDictionary[kPubDateKey];
        self.linkString = aDictionary[kGuidKey];
        
        NSDictionary *theThumbnailDictionary = aDictionary[kMediaThumbnailKey];
        self.thumbnailURLString = theThumbnailDictionary[kURLKey];
        self.thumbnailWidth = [theThumbnailDictionary[kWidthKey] floatValue];
        self.thumbnailHeight = [theThumbnailDictionary[kHeightKey] floatValue];
    }

    return self;    
}

- (instancetype)initWithFeedItemManagedObject:(FeedItemManagedObject *)anObject
{
	self = [super init];
    
    if (nil != self)
    {
    	self.title = anObject.title;
        self.descriptionText = anObject.descriptionText;
        self.pubDate = anObject.pubDate;
        self.linkString = anObject.linkString;;
        self.thumbnailURLString = anObject.thumbnailURLString;
        self.thumbnailWidth = [anObject.thumbnailWidth floatValue];
        self.thumbnailHeight = [anObject.thumbnailHeight floatValue];
        self.imageData = anObject.imageData;
    }

    return self;
}

@end
