//
//  NewsTableViewCell.m
//  TestRSSReader
//
//  Created by Artyom Belenkov on 3/1/16.
//  Copyright © 2016 abelenkov. All rights reserved.
//

#import "NewsTableViewCell.h"

@interface NewsTableViewCell ()

@property (strong, nonatomic) IBOutlet UIImageView *thumbnail;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *imageWidthConstraint;

@end

@implementation NewsTableViewCell

@synthesize thumbnailImage;

+ (BOOL)requiresConstraintBasedLayout
{
	return YES;
}

- (void)setThumbnailImage:(UIImage *)aThumbnailImage
{
	if (self.thumbnail.image != aThumbnailImage)
    {
        self.imageWidthConstraint.constant = aThumbnailImage.size.width;
        [self setNeedsUpdateConstraints];
        
    	self.thumbnail.image = aThumbnailImage;
        [self.activityIndicator stopAnimating];
    }
}

- (void)updateConstraints
{
	self.titleLabel.preferredMaxLayoutWidth = self.titleLabel.frame.size.width;
    self.descriptionTextLabel.preferredMaxLayoutWidth =
    		self.descriptionTextLabel.frame.size.width;
    
	[super updateConstraints];
}

- (void)prepareForReuse
{
	[self.activityIndicator stopAnimating];
}

@end
