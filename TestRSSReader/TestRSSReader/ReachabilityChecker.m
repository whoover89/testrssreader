//
//  ReachabilityChecker.m
//  TestRSSReader
//
//  Created by Artyom Belenkov on 3/2/16.
//  Copyright © 2016 abelenkov. All rights reserved.
//

#import "ReachabilityChecker.h"
#import <SystemConfiguration/SCNetworkReachability.h>

@interface ReachabilityChecker ()

@property (nonatomic, strong) NSTimer *timer;

@end

@implementation ReachabilityChecker

NSString *const kReachabilityNotification = @"kReachabilityNotification";
NSString *const kReachableKey = @"kReachableKey";

+ (ReachabilityChecker *)sharedInstance
{
    static ReachabilityChecker *theInstance = nil;
    static dispatch_once_t theOnceToken;
    
    dispatch_once(&theOnceToken,
    ^{
        theInstance = [ReachabilityChecker new];
    });
    
    return theInstance;
}

+ (BOOL)checkNetworkAvailable
{
    SCNetworkReachabilityFlags theFlags;
    SCNetworkReachabilityRef theAddress;
    theAddress = SCNetworkReachabilityCreateWithName(NULL, "www.apple.com");
    BOOL theSuccess = SCNetworkReachabilityGetFlags(theAddress, &theFlags);
    CFRelease(theAddress);

    return (theSuccess && !(theFlags &
    		kSCNetworkReachabilityFlagsConnectionRequired) &&
            (theFlags & kSCNetworkReachabilityFlagsReachable));
}

- (void)dealloc
{
	[self stopChecking];
}

- (void)startChecking
{
	self.shouldNotify = [[self class] checkNetworkAvailable];
	self.timer = [NSTimer timerWithTimeInterval:2.0 target:self selector:
    		@selector(verifyReachability) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:self.timer
    		forMode:NSDefaultRunLoopMode];
}

- (void)stopChecking
{
	[self.timer invalidate];
    self.timer = nil;
}

- (void)verifyReachability
{
	BOOL theReachable = [[self class] checkNetworkAvailable];
	if (self.shouldNotify != theReachable)
    {
    	self.shouldNotify = theReachable;
    	[self notifyReachabilityDidChange:theReachable];
    }
}

- (void)notifyReachabilityDidChange:(BOOL)aReachability
{
	[[NSNotificationCenter defaultCenter] postNotificationName:
    		kReachabilityNotification object:nil userInfo:
            @{kReachableKey : @(aReachability)}];
}

@end
